# Future Coder and Impactbyte - Course: Fullstack app
<p style="text-align:center;">
<center>
  <img src="./assets/logo.png">
</center>


Hello Students! Welcome to the fullstack weekend course from Impactbyte and Future Coder.

---
# Design
<center>
  <img src="./assets/wireframe-mockup.png">
</center>

# Wireframe vs Mockup vs Prototype

Commonly when designing a website or application, we are recommended to visualize it first.
## Wireframe 
Wireframing is the easiest design method to create just the essentials. Plan out the content, positioning, and more.
## Mockup 
Mockup is the bare minimum of the design. We can see better layout, colors, typography, images, and more.
Prototype is often the advanced one where we can make the mockup interactively used. Without having to code it.

## Example 
<center>
  <img src="./assets/example.png">
</center>

Now [here's](https://www.figma.com/proto/drXSiBtm3RuBsJwUUKJ7CAK1/Todoapp?node-id=0%3A1&scaling=min-zoom) an example of an interactive prototype of the app we're building. You can play around by clicking the highlighted areas: input box, add button, and delete button.

## Design Tools

Nowadays, we can just use online design tool to create a mockup or graphic design. Let's use [Figma](https://www.figma.com) for a start, and follow along with the predefined design wireframe/mockup for our app.

# React.js UI Library
React is described as a JavaScript library for building user interfaces (taken from the official documentation).

- With 3 main features:
    * Declarative 
    React makes it painless to create interactive UIs. Design simple views for each state in your application, and React will efficiently update and render just the right components when your data changes. Declarative views make your code more predictable and easier to debug.
    * Component-Based: Build encapsulated components that manage their own state, then compose them to make complex UIs. Since component logic is written in JavaScript instead of templates, you can easily pass rich data through your app and keep state out of the DOM.
    * Learn Once, Write Anywhere: We don’t make assumptions about the rest of your technology stack, so you can develop new features in React without rewriting existing code. React can also render on the server using Node and power mobile apps using React Native.

# Website structure
Let's think in component-based design and development.

# React Basics
Who uses React? A lot. Especially Facebook, Instagram, Airbnb, etc.

The main reasons and philosophy of React is to improve both developer and designer productivity, by using a component-based approach when building a complex web application.

The important thing about React is that it uses: Component and virtual DOM.

What if we compare React vs jQuery, Vue, Angular, Preact, Backbone, Web Components, Elm, etc?
By convention, creating components and writing HTML in React is using JSX (JavaScript Extension) syntax.
The main component can be either written using class or plain function.

JSX is a DSL (Domain Specific Language) over JavaScript. So it's not plain programming language.
Should we always React? Depends. If our application has some complexity, it's recommended. But if it's just a simple website, just use plain HTML, CSS, JavaScript & jQuery like usual.

# React JSX
JSX commonly looks like this. It looks like combining HTML inside JavaScript. But in reality, they're just an alternative way to write JavaScript functions.

in JSX:
~~~~
return (
  <div>
    <h1 className="app-title">App Name</h1>
    <p>{todoList}</p>
  </div>
)
~~~~
In plain JavaScript
~~~~~
return React.createElement(
  "div",
  null,
  React.createElement("h1", { className: "app-title" }, "App Name"),
  React.createElement("p", null, todoList)
)
~~~~~

# React Installation
~~~~
npm install -g create-react-app
~~~~

~~~~
create-react-app example
cd example
npm start
~~~~

~~~~~
├── node_modules
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
├── README.md
├── src
│   ├── App.css
│   ├── App.js
│   ├── App.test.js
│   ├── index.css
│   ├── index.js
│   ├── logo.svg
│   └── registerServiceWorker.js
└── yarn.lock
~~~~~

# React data
Import modules or another components
JSX className vs HTML class
state, setState, getState
initial state (this.state) vs changed state (this.setState())
props, propTypes, and defaultProps
props can be accessed in the children component like so:

~~~~~
<div index={props.index}>{props.children}</div>
~~~~~

# React state
Passing state to child component

# React props
Props as a static data medium that passed in from component to component.

We can get props from parent component then use it in child component as props or props.children

# React Method
Custom method and .bind(this) in constructor.

~~~~~Javascript
class Name extends React.Component {
  constructor(props) {
    super(props)

    this.actionName = this.actionName.bind(this)
  }

  actionName() {
    console.log("Violla!")
  }

  render() {
    return <div onClick={this.actionName}>Box</div>
  }
}
~~~~~

If we want to use a parameter in the method, don't call it directly.

~~~~~jsx
/* do this */
<div
  onClick={() => {
    this.actionName(param)
  }}
/>

/* not this */
<div onClick={this.actionName(param)} />
~~~~~

# React Environment
By default, <span style="background-color: rgb(66, 244, 226)">react-scripts</span> in <span style="background-color: rgb(66, 244, 226)">create-react-app</span>  support environment variable through .env file.

We can define such <span style="background-color: rgb(66, 244, 226)">REACT_APP_VARIABLE</span>  then access it as <span style="background-color: rgb(66, 244, 226)">process.env.REACT_APP_VARIABLE</span>  

~~~~js
REACT_APP_VARIABLE=example
REACT_APP_NAME=impactodo
REACT_APP_API_URL=http://localhost:3000
~~~~

~~~~js
const VARIABLE = process.env.REACT_APP_VARIABLE
const NAME = process.env.REACT_APP_NAME
const API_URL = process.env.REACT_APP_API_URL
~~~~
# Reactstrap
This library contains React Bootstrap 4 components that favor composition and control. The library does not depend on jQuery or Bootstrap javascript. 

There are a few core concepts to understand in order to make the most out of this library.
Your content is expected to be composed via props.children rather than using named props to pass in Components.
~~~~js
// Content passed in via props
const Example = (props) => {
  return (
    <p>This is a tooltip <TooltipTrigger tooltip={TooltipContent}>example</TooltipTrigger>!</p>
  );
}

// Content passed in as children (Preferred)
const PreferredExample = (props) => {
  return (
    <p>
      This is a <a href="#" id="TooltipExample">tooltip</a> example.
      <Tooltip target="TooltipExample">
        <TooltipContent/>
      </Tooltip>
    </p>
  );
}
~~~~



# React Form
Form handling in React is actually pretty simple.

* Get value from form inputs when event onChange, onClick, or onSubmit is triggered.
* Set a new state with setState based on the event.target.value when event onClick or onSubmit is triggered again.

Plain HTML:
~~~~html
<form>
  <label>
    Name:
    <input type="text" name="name" />
  </label>
  <input type="submit" value="Submit" />
</form>
~~~~

Usage in React with JSX:

~~~~jsx
class SimpleForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = { value: "" }

    this.handleChange = this.handleChange.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(event) {
    this.setState({ value: event.target.value })
  }

  handleSubmit(event) {
    alert("A name was submitted: " + this.state.value)
    event.preventDefault()
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
          />
        </label>
        <input type="submit" value="Submit" />
      </form>
    )
  }
}
~~~~

# Axios

Axios is a Promise-based HTTP client for JavaScript which can be used in your front-end application and in your Node.js backend. 

By using Axios it’s easy to send asynchronous HTTP request to REST endpoints and perform CRUD operations. The Axios library can be used in your plain JavaScript application or can be used together with more advanced frameworks like React / React Native

~~~~js
$ npm install axios
~~~~

Example 
~~~~js
axios.get('/user?ID=12345')
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
  ~~~~~


# Express JS
- [ ] Express Essentials
    * IP address and port
    * Ignore templating engine

- [ ] Express Routing

    <span style="background-color: rgb(66, 244, 226)">GET</span>,  <span style="background-color: rgb(66, 244, 226)">POST</span>, <span style="background-color: rgb(66, 244, 226)">PUT</span>, <span style="background-color: rgb(66, 244, 226)">DELETE</span>

- [ ] Express Data

    <span style="background-color: rgb(66, 244, 226)">body</span>,  <span style="background-color: rgb(66, 244, 226)">params</span>, <span style="background-color: rgb(66, 244, 226)">query</span>

- [ ] Node.js, Browser, & REST API
    * fetch
    * axios

- [ ] Reference
    * [node-fetch](https://www.npmjs.com/package/node-fetch)
    * [axios](https://www.npmjs.com/package/axios)

    # Express Cheatsheet
    ~~~~js
    npm init
    npm install --save express // Install express

    ~~~~

    ## Express Structure

    ~~~~js
    // Require express package
    const express = require("express")

    // Initialize express
    const app = express()

    // Create our first route
    app.get("/", (req, res) => {
    res.send("Hello World") // Send `Hello World` to client
    })

    // Start server at port 3000
    app.listen(3000)

    ~~~~

    result :
    <center>
  <img src="./assets/result.png">
</center>

# Express Generator
Use the application generator tool, express-generator, to quickly create an application skeleton.

The express-generator package installs the express command-line tool. Use the following command to do so:

~~~~js
npm install express-generator -g
~~~~

For example, the following creates an Express app named myapp. The app will be created in a folder named myapp in the current working directory and the view engine will be set to default:

~~~~js
express Impactbytetodo
~~~~

<center>
  <img src="./assets/express.png">
</center>


# Database NoSQL
## Non-Relational Database
### NoSQL

NoSQL (not only SQL) database provides a mechanism for storage and retrieval of data that is modeled in other than using relational way.
Data manipulation is often done through object-oriented functions or APIs. There's no specific query language like SQL.
    <center>
  <img src="./assets/nosql.png">
</center>

### NoSQL Database Types

**Document databases**: pair each key with a complex data structure known as a document. Documents can contain many different key-value pairs, or key-array pairs, or even nested documents.

**Graph stores**: are used to store information about networks of data, such as social connections. Graph stores include Neo4J and Giraph.

**Key-value stores**: are the simplest NoSQL databases. Every single item in the database is stored as an attribute name (or 'key'), together with its value. Examples of key-value stores are Riak and Berkeley DB. Some key-value stores, such as Redis, allow each value to have a type, such as 'integer', which adds functionality.

**Wide-column stores**: such as Cassandra and HBase are optimized for queries over large datasets, and store columns of data together, instead of rows.

 <center>
  <img src="./assets/mongodb.png">
</center>

### Examples
* Document
    - MongoDB
    - Firebase
    - ArangoDB
    - CouchDB
    - PouchDB
    - NeDB

# MongoDB
### Installation and Usage
For detailed guidance, refer to needed documentation on each database.

* [Install mongodb community edition](https://docs.mongodb.com/v3.2/administration/install-community)

# Database NoSQL ODM
## Mongoose

* Install mongoose on your dependency
~~~~
npm install mongoose --save
~~~~
Now say we like fuzzy kittens and want to record every kitten we ever meet in MongoDB. The first thing we need to do is include mongoose in our project and open a connection to the test database on our locally running instance of MongoDB.


* Importing Mongoose to your project
~~~~
/ Using Node.js `require()`
const mongoose = require('mongoose');
 
// Using ES6 imports
import mongoose from 'mongoose';
~~~~

* Connecting to MongoDB

First, we need to define a connection. If your app uses only one database, you should use <span style="background-color: rgb(66, 244, 226)">mongoose.connect.</span> If you need to create additional connections, use <span style="background-color: rgb(66, 244, 226)">mongoose.createConnection.</span>

Both connect and createConnection take a <span style="background-color: rgb(66, 244, 226)">mongodb:// URI, or the parameters host, database, port, options.</span> 

~~~~js
const mongoose = require('mongoose');
 
mongoose.connect('mongodb://localhost/my_database');
~~~~

### Defining a Model
Models are defined through the Schema interface.

~~~~js 
const ImpactbyteTodo = new Schema({
  Task: String
});
~~~~

# Example 
* Cretae directory **models** on root directory
 <center>
  <img src="./assets/models.png">
</center>

* create file Task.js in directory models
<center>
  <img src="./assets/task.png">
</center>

* Create Task Schema using mongoose in Task.js

~~~~js
const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TaskSchema = new Schema({
    task : {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now
    },
    updatedAt: {
        type: Date,
        required: true,
        default: Date.now
    }
})
module.exports = mongoose.model('Task', TaskSchema)
~~~~

* Create directory **controllers**
<center>
  <img src="./assets/controllers.png">
</center>

* Create file Task.js in directory **controllers**
<center>
  <img src="./assets/task-controller.png">
</center>

* Create task controller in Task.js
~~~~js
const Task = require('./models/Task')

module.exports = {
  //Post data to database
    Create: async (req, res, next) => {
        try {
            const {task} = req.body
            const Newtask = new Task({task})
            await Newtask.save()
            res
                .status(200)
                .json({Newtask})
        } catch (err) {
            next(err)
        }

    },
  
  //get all data from database
    GetAll: async (req, res, next) => {
        try {
            const Tasks = await Task.find({})
            res
                .status(200)
                .json({Tasks})
        } catch (err) {
            next(err)
        }
    },
// update data by id
    Update: async (req, res, next) => {
        let task = {task: req.body.task}
        try {
            const Tasks = await Task.findOneAndUpdate({_id: req.params.taskId}, task, {new: true})
            res.status(200).json({Tasks})
        } catch (err) {
            next(err)
        }
    },
// delete data by id
    DeleteById: async (req, res, next) => {
        try {
            await Task.findOneAndRemove({_id: req.params.taskId})
            res.status(200).json({Message: 'Delete Successfully'})
        } catch (err) {
            next(err)
        }
    }


}
~~~~

# Dotenv
Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env. Storing configuration in the environment separate from code is based on [The Twelve-Factor App methodology](https://12factor.net/config)

## install :
~~~~
npm install dotenv
~~~~

## Usage
As early as possible in your application (app.js), require and configure dotenv.
~~~~js
require('dotenv').config()
~~~~

Create a .env file in the root directory of your project. Add environment-specific variables on new lines in the form of NAME=VALUE. For example: 
~~~~js
MONGOLAB_URI="mongodb://localhost:27017/impactbytetodo"
~~~~

connect you application (app.js) in mongodb :
~~~~js
const mongodConnect = process.env.MONGOLAB_URI
mongoose.connect(mongodConnect)
~~~~

